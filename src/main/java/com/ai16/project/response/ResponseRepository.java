package com.ai16.project.response;

import com.ai16.project.consultation.Consultation;
import com.ai16.project.websiteUser.WebsiteUser;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ResponseRepository extends JpaRepository<Response, Long> {
    Response findByFkuserAndFkconsultation(WebsiteUser Fkuser, Consultation Fkconsultation);
    Response findById(int id);
    List<Response> findAllByFkuser(WebsiteUser Fkuser);
    List<Response> findAllByFkconsultation(Consultation Fkconsultation);
    List<Response> getAllBy();

}
