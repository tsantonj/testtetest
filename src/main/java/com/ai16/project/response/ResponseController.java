package com.ai16.project.response;

import com.ai16.project.consultation.Consultation;
import com.ai16.project.consultation.ConsultationRepository;
import com.ai16.project.proposition.Proposition;
import com.ai16.project.proposition.PropositionRepository;
import com.ai16.project.subject.SubjectRepository;
import com.ai16.project.websiteUser.WebsiteUser;
import com.ai16.project.websiteUser.WebsiteUserRepository;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/yfc_utc/responses")
public class ResponseController {
    private ResponseRepository responseRepository;
    private ConsultationRepository consultationRepository;
    private PropositionRepository propositionRepository;
    private SubjectRepository subjectRepository;
    private WebsiteUserRepository userRepository;

    public ResponseController(ResponseRepository responseRepository, ConsultationRepository consultationRepository, SubjectRepository subjectRepository, PropositionRepository propositionRepository, WebsiteUserRepository userRepository) {
        this.responseRepository = responseRepository;
        this.consultationRepository = consultationRepository;
        this.propositionRepository = propositionRepository;
        this.subjectRepository = subjectRepository;
        this.userRepository = userRepository;
    }
/*
    @GetMapping (value = "/user")
    public List<Response> getAllResponses(@RequestBody String Fkuser) {
        JSONObject jsonObject = new JSONObject(Fkuser);
        WebsiteUser author = userRepository.findByUsername(jsonObject.getString("author"));

        List<Response> responseList = responseRepository.findAllByFkuser(author);

        return responseList;
    }

    @GetMapping (value = "/user/{id]")
    public List<Response> getAllResponses(@PathVariable("id") int id) {
        WebsiteUser author = userRepository.findById(id);
        List<Response> responseList = responseRepository.findAllByFkuser(author);

        for (int i = 0; i < responseList.size(); i++) {
            System.out.println(responseList.get(i));
            System.out.println(responseList.get(i).getChoice());
            System.out.println(responseList.get(i).getComment());
            System.out.println(responseList.get(i).getFkconsultation());
        }

        return responseList;
    }
    */

    /*
        get all responses
     */
    @GetMapping(value = "")
    public List<Response> get() {
        List<Response> responses = responseRepository.getAllBy();

        return responses;
    }

    /*
        get a response by its id
     */
    @GetMapping(value = "/{id}")
    public Response get(@PathVariable("id") int id) {
        return responseRepository.findById(id);
    }

    /*
        Post many responses
     */
    @PostMapping(value = "")
    public List<Response> postSome(@RequestBody String toParse) {
        JSONArray jsonArray = new JSONArray(toParse);
        List<Response> responses = new ArrayList<>();


        for (int i = 0; i < jsonArray.length(); i++) {
            JSONObject jsonObject = jsonArray.getJSONObject(i);
            System.out.println(jsonObject);
            WebsiteUser author = userRepository.findByUsername(jsonObject.getString("author"));
            Proposition proposition = propositionRepository.findByName(jsonObject.getString("proposition"));
            Consultation consultation = consultationRepository.findByFkuserAndFkproposition(author, proposition);
            String comment = jsonObject.getString("comment");
            String choice = jsonObject.getString("choice");

            if (choice == "accepted" || choice == "refused") {
                Response response = new Response(consultation, author, choice, comment);
                responseRepository.save(response);
                responses.add(response);
            } else {
                System.err.println("erreur, choice doit etre accepted or refused");
            }
        }

        return responses;
    }

    /*
        Post a response by its id
     */
    @PostMapping(value = "/response")
    public Response post(@RequestBody String toParse) {
        JSONObject jsonObject = new JSONObject(toParse);

        WebsiteUser author = userRepository.findByUsername(jsonObject.getString("author"));
        Proposition proposition = propositionRepository.findByName(jsonObject.getString("proposition"));

        Consultation new_consultation = new Consultation(author, proposition);
        consultationRepository.save(new_consultation);
        Consultation consultation = consultationRepository.findByFkuserAndFkproposition(author, proposition);
        String comment = jsonObject.getString("comment");
        String choice = jsonObject.getString("choice");
        if (choice == "accepted" || choice == "refused") {

            Response response = new Response(consultation, author, choice, comment);

            responseRepository.save(response);

            return response;

        } else {
            System.err.println("erreur, choice doit etre accepted or refused");
            return null;
        }
    }


    @DeleteMapping(value = "/{id}")
    //@Query("delete from websiteuser u where i.id=:id")
    public void delete(@PathVariable("id") Integer id) {
        responseRepository.delete(responseRepository.findById(id));
    }
}
