package com.ai16.project.response;

import com.ai16.project.consultation.Consultation;
import com.ai16.project.websiteUser.WebsiteUser;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
//@Embeddable
@Table (name = "response")
public class Response {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    @ManyToOne
    @NotNull
    @JoinColumn(name = "fkconsultation")
    private Consultation fkconsultation;
    @ManyToOne
    @NotNull
    @JoinColumn(name = "fkuser")
    private WebsiteUser fkuser;
    private String choice;
    private String comment;

    public Response(Consultation consultation, WebsiteUser user, String comment) {
        this.fkconsultation = consultation;
        this.fkuser = user;
        this.comment = comment;
    }

    public Response() {
        // NOPE
    }

    public Response(Consultation consultation, WebsiteUser user, String choice, String comment) {
        this.fkconsultation = consultation;
        this.fkuser = user;
        this.choice = choice;
        this.comment = comment;
    }

    public Consultation getFkconsultation() {
        return fkconsultation;
    }

    public WebsiteUser getFkuser() {
        return fkuser;
    }

    public String getChoice() {
        return choice;
    }

    public String getComment() {
        return comment;
    }

    public int getId() {
        return id;
    }
}
