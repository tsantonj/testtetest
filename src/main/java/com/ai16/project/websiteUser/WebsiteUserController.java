package com.ai16.project.websiteUser;

import com.ai16.project.consultation.Consultation;
import com.ai16.project.consultation.ConsultationRepository;
import com.ai16.project.proposition.Proposition;
import com.ai16.project.proposition.PropositionRepository;
import com.ai16.project.response.Response;
import com.ai16.project.response.ResponseRepository;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;
import java.util.*;

@RestController
@RequestMapping(value = "/yfc_utc/users")
public class WebsiteUserController {
    private ConsultationRepository consultationRepository;
    private WebsiteUserRepository websiteUserRepository;
    private PropositionRepository propositionRepository;
    private ResponseRepository responseRepository;
    /*
    @Autowired
    private PasswordEncoder passwordEncoder;
    */
    public WebsiteUserController(WebsiteUserRepository websiteUserRepository, PropositionRepository propositionRepository, ResponseRepository responseRepository, ConsultationRepository consultationRepository) {
        this.websiteUserRepository = websiteUserRepository;
        this.propositionRepository = propositionRepository;
        this.responseRepository = responseRepository;
        this.consultationRepository = consultationRepository;
    }

    /*@Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }*/
/*
    @PostMapping(value = "/all")
    public void post(@RequestBody String toParse) {
        JSONObject jsonObject = new JSONObject(toParse);

        String username = jsonObject.getString("username");
        // https://www.baeldung.com/spring-security-registration-password-encoding-bcrypt
        String password = jsonObject.getString("password");//(passwordEncoder.encode(jsonObject.getString("password")));
        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        String password_hash = passwordEncoder.encode(password);

        WebsiteUser new_user = new WebsiteUser(username, password_hash);
        new_user.setUsertype("User");
        new_user.setState("Active");
        new_user.setAddress(password);

        websiteUserRepository.save(new_user);
    }

    @PostMapping(value = "/many")
    public void postMany(@RequestBody String toParse) {
        JSONArray jsonArray = new JSONArray(toParse);

        for (int i = 0; i < jsonArray.length(); i++) {
            JSONObject jsonObject = jsonArray.getJSONObject(i);
            String username = jsonObject.getString("username");
            String password = jsonObject.getString("password");

            WebsiteUser new_user = new WebsiteUser(username, password);
            new_user.setUsertype("User");
            new_user.setState("Active");

            websiteUserRepository.save(new_user);
        }
    }

    @GetMapping(value = "/all")
    public WebsiteUser getUser(@RequestBody String toParse) {
        JSONObject jsonObject = new JSONObject(toParse);

        String username = jsonObject.getString("username");

        WebsiteUser user = websiteUserRepository.findByUsername(username);
        WebsiteUser userWOutPassword = new WebsiteUser();

        userWOutPassword.setState(user.getState());
        userWOutPassword.setAddress(user.getAddress());
        userWOutPassword.setUsertype(user.getUsertype());
        userWOutPassword.setPhonenumber(user.getPhonenumber());
        userWOutPassword.setCreationDate(user.getCreationdate());
        userWOutPassword.setFirstname(user.getFirstname());
        userWOutPassword.setLastname(user.getLastname());
        userWOutPassword.setUsername(user.getUsername());

        return userWOutPassword;
    }
    /*
    @GetMapping(value = "/all")
    public List<WebsiteUser> get() {
        return websiteUserRepository.findAll();
    }*/


    //////////////////////////////////////////////



    /*
        get all users
     */
    @GetMapping(value = "")
    public List<WebsiteUser> get() {
        List<WebsiteUser> users = websiteUserRepository.getAllBy();

        return users;
    }

    /*
        get a subject by its id
     */
    @GetMapping(value = "/{id}")
    public WebsiteUser get(@PathVariable("id") int id) {
        return websiteUserRepository.findById(id);
    }

    /*
        get all propositions linked to that user
     */
    @GetMapping(value = "/{id}/propositions")
    public List<Proposition> getPropositions(@PathVariable("id") int id) {
        WebsiteUser user = websiteUserRepository.findById(id);
        List<Proposition> propositions = propositionRepository.findAllByFkauthor(user);

        return propositions;
    }

    /*
        get all propositions linked to that user
     */
    @GetMapping(value = "/{id}/responses")
    public List<Response> getResponses(@PathVariable("id") int id) {
        WebsiteUser user = websiteUserRepository.findById(id);
        List<Response> responses = responseRepository.findAllByFkuser(user);

        return responses;
    }

    /*
        get all propositions linked to that user
     */
    @GetMapping(value = "/{id}/consultations")
    public List<Consultation> getConsultations(@PathVariable("id") int id) {
        WebsiteUser user = websiteUserRepository.findById(id);
        List<Consultation> consultations = consultationRepository.findAllByFkuser(user);

        return consultations;
    }

    /*
        Post many subjects
     */
    @PostMapping(value = "")
    public List<WebsiteUser> postSome(@RequestBody String toParse) {
        JSONArray jsonArray = new JSONArray(toParse);
        List<WebsiteUser> users = new ArrayList<>();

        for (int i = 0; i < jsonArray.length(); i++) {
            JSONObject jsonObject = jsonArray.getJSONObject(i);

            String username = jsonObject.getString("username");
            if (websiteUserRepository.findByUsername(username) != null) {
                System.err.println("This user already exists : " + username);
            } else {
                String password = jsonObject.getString("password");
                BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
                String password_hash = passwordEncoder.encode(password);

                WebsiteUser user = new WebsiteUser(username, password_hash);
                user.setUsertype("User");
                user.setState("Active");
                user.setAddress(password);

                websiteUserRepository.save(user);
                users.add(user);
            }
        }

        return users;
    }

    /*
        Post a subject by its id
        https://www.baeldung.com/spring-security-registration-password-encoding-bcrypt

     */
    @PostMapping(value = "/user")
    public WebsiteUser post(@RequestBody String toParse) {
        JSONObject jsonObject = new JSONObject(toParse);
        System.out.println(jsonObject);
        String username = jsonObject.getString("username");
        if (websiteUserRepository.findByUsername(username) != null) {
            System.err.println("This user already exists : " + username);
            return null;
        } else {
            String password = jsonObject.getString("password");
            BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
            String password_hash = passwordEncoder.encode(password);

            WebsiteUser user = new WebsiteUser(username, password_hash);
            user.setUsertype("User");
            user.setState("Active");
            user.setAddress(password);

            websiteUserRepository.save(user);
            return user;
        }
    }

    @PutMapping(value = "")
    public List<WebsiteUser> putMany(@RequestBody String toParse) {
        JSONArray jsonArray = new JSONArray(toParse);
        List<WebsiteUser> users = new ArrayList<>();

        for (int i = 0; i < jsonArray.length(); i++) {
            JSONObject jsonObject = jsonArray.getJSONObject(i);

            String username = jsonObject.getString("username");
            String password = jsonObject.getString("password");
            BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
            password = passwordEncoder.encode(password);
            String state = jsonObject.getString("state");
            String type = jsonObject.getString("type");
            String adr = jsonObject.getString("address");
            String fname = jsonObject.getString("firstname");
            String lname = jsonObject.getString("lastname");
            String phone = jsonObject.getString("phone");

            WebsiteUser user = websiteUserRepository.findByUsername(username);
            user.setUsertype(type);
            user.setState(state);
            user.setPassword(password);
            user.setAddress(adr);
            user.setFirstname(fname);
            user.setLastname(lname);
            user.setPhonenumber(phone);

            websiteUserRepository.save(user);
        }

        return users;
    }

    @PutMapping(value = "/{id}")
    public WebsiteUser put(@PathVariable("id") String id, @RequestBody String toParse) {
        WebsiteUser user;

        if (id.matches("\\d+")) {
            user =  websiteUserRepository.findById(Integer.parseInt(id));
        } else {
            user =  websiteUserRepository.findByUsername(id);
        }
        JSONObject jsonObject = new JSONObject(toParse);

        String username = jsonObject.getString("username");
        String password = jsonObject.getString("password");
        if (password.length() < 6) {
            System.err.println("Mot de passe de 6 caractères requis");
            return null;
        }
        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        password = passwordEncoder.encode(password);
        String state = jsonObject.getString("state");
        String type = jsonObject.getString("type");
        String adr = jsonObject.getString("address");
        String fname = jsonObject.getString("firstname");
        String lname = jsonObject.getString("lastname");
        String phone = jsonObject.getString("phone");

        user.setUsertype(type);
        user.setState(state);
        user.setPassword(password);
        user.setAddress(adr);
        user.setFirstname(fname);
        user.setLastname(lname);
        user.setPhonenumber(phone);

        websiteUserRepository.save(user);

        return user;
    }


    @DeleteMapping(value = "/{id}")
    //@Query("delete from websiteuser u where i.id=:id")
    public void delete(@PathVariable("id") Integer id) {
        websiteUserRepository.delete(websiteUserRepository.findById(id));
    }
}
