package com.ai16.project.websiteUser;

import javax.persistence.*;
import java.sql.Date;
import java.time.LocalDate;

@Entity
@Table(name = "Websiteuser")
public class WebsiteUser {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    private String username;
    private String password;
    private String address;
    private String usertype;
    private String firstname;
    private String lastname;
    private String phonenumber;
    private Date creationDate;
    private String state;

    public WebsiteUser() {
    }

    public WebsiteUser(String username, String password) {
        this.username = username;
        this.password = password;
        LocalDate date = LocalDate.now();
        this.creationDate = Date.valueOf(date);
    }

    public String getUsername() {
        return username;
    }
    public String getPassword() {
        return password;
    }
    public String getAddress() {
        return address;
    }
    public String getUsertype() {
        return usertype;
    }
    public String getFirstname() {
        return firstname;
    }
    public String getLastname() {
        return lastname;
    }
    public String getPhonenumber() {
        return phonenumber;
    }
    public Date getCreationdate() {
        return creationDate;
    }
    public String getState() {
        return state;
    }

    public void setId(int id) { this.id = id; }
    public void setUsername(String username) { this.username = username; }
    public void setPassword(String password) { this.password = password; }
    public void setAddress(String address) { this.address = address; }
    public void setUsertype(String usertype) { this.usertype = usertype; }
    public void setFirstname(String firstname) { this.firstname = firstname; }
    public void setLastname(String lastname) { this.lastname = lastname; }
    public void setPhonenumber(String phonenumber) { this.phonenumber = phonenumber; }
    public void setCreationDate(Date creationDate) { this.creationDate = creationDate; }
    public void setState(String state) { this.state = state; }
}
