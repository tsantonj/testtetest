package com.ai16.project.websiteUser;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface WebsiteUserRepository extends JpaRepository<WebsiteUser, Long> {
    WebsiteUser findByUsername(String username);
    WebsiteUser findById(int id);
    WebsiteUser findByPhonenumber(String phonenumber);
    WebsiteUser findByFirstnameAndLastname (String firstname, String lastname);
    List<WebsiteUser> findAllByState(String state);
    List<WebsiteUser> getAllBy();

}
