package com.ai16.project.proposition;

import com.ai16.project.consultation.Consultation;
import com.ai16.project.consultation.ConsultationRepository;
import com.ai16.project.subject.Subject;
import com.ai16.project.subject.SubjectRepository;
import com.ai16.project.websiteUser.WebsiteUser;
import com.ai16.project.websiteUser.WebsiteUserRepository;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/yfc_utc/propositions")
public class PropositionController {
    private PropositionRepository propositionRepository;
    private SubjectRepository subjectRepository;
    private WebsiteUserRepository websiteUserRepository;
    private ConsultationRepository consultationRepository;

    public PropositionController (ConsultationRepository consultationRepository, PropositionRepository propositionRepository, WebsiteUserRepository websiteUserRepository, SubjectRepository subjectRepository) {
        this.propositionRepository = propositionRepository;
        this.subjectRepository = subjectRepository;
        this.websiteUserRepository = websiteUserRepository;
        this.consultationRepository = consultationRepository;
    }


    /*
        get all propositions
     */
    @GetMapping(value = "")
    public List<Proposition> get() {
        List<Proposition> propositions = propositionRepository.getAllBy();

        return propositions;
    }

    /*
        get a proposition by its id
     */
    @GetMapping(value = "/{id}")
    public Proposition get(@PathVariable("id") int id) {
        return propositionRepository.findById(id);
    }

    /*
        Post many propositions
     */
    @PostMapping(value = "")
    public List<Proposition> postSome(@RequestBody String toParse) {
        JSONArray jsonArray = new JSONArray(toParse);
        List<Proposition> propositions = new ArrayList<>();


        for (int i = 0; i < jsonArray.length(); i++) {
            JSONObject jsonObject = jsonArray.getJSONObject(i);

            String name = jsonObject.getString("name");
            Subject subject = subjectRepository.findByName(jsonObject.getString("subject"));
            WebsiteUser username = websiteUserRepository.findByUsername(jsonObject.getString("username"));

            if (propositionRepository.findByName(name) == null) {
                Proposition proposition = new Proposition(username, subject, name);
                propositionRepository.save(proposition);
                propositions.add(proposition);


                // s'il il post, c'est qu'il l'a consulté x)
                Consultation consultation = new Consultation(username, proposition);
                consultationRepository.save(consultation);
            }
        }

        return propositions;
    }

    /*
        Post a response by its id
     */
    @PostMapping(value = "/proposition")
    public Proposition post(@RequestBody String toParse) {
        JSONObject jsonObject = new JSONObject(toParse);

        String name = jsonObject.getString("name");
        Subject subject = subjectRepository.findByName(jsonObject.getString("subject"));
        WebsiteUser username = websiteUserRepository.findByUsername(jsonObject.getString("username"));

        if (propositionRepository.findByName(name) == null) {
            Proposition proposition = new Proposition(username, subject, name);
            propositionRepository.save(proposition);

            // s'il il post, c'est qu'il l'a consulté x)
            Consultation consultation = new Consultation(username, proposition);
            consultationRepository.save(consultation);

            return proposition;
        } else {
            return null;
        }
    }

    @PutMapping(value = "")
    public List<Proposition> putMany(@RequestBody String toParse) {
        JSONArray jsonArray = new JSONArray(toParse);
        List<Proposition> propositions = new ArrayList<>();
        Proposition proposition = null;

        for (int i = 0; i < jsonArray.length(); i++) {
            JSONObject jsonObject = jsonArray.getJSONObject(i);
            proposition = propositionRepository.findByName(jsonObject.getString("name"));

            proposition.setComment(jsonObject.getString("comment"));
            proposition.setState(jsonObject.getString("state"));

            propositionRepository.save(proposition);
            propositions.add(proposition);
        }

        return propositions;
    }

    @PutMapping(value = "/{id}")
    public Proposition put(@PathVariable("id") int id, @RequestBody String toParse) {
        Proposition proposition =  propositionRepository.findById(id);
        JSONObject jsonObject = new JSONObject(toParse);

        proposition.setComment(jsonObject.getString("comment"));
        proposition.setState(jsonObject.getString("state"));

        propositionRepository.save(proposition);

        return proposition;
    }

}
