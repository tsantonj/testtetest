package com.ai16.project.proposition;

import com.ai16.project.subject.Subject;
import com.ai16.project.websiteUser.WebsiteUser;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface PropositionRepository extends JpaRepository<Proposition, Long> {
    Proposition findByFksubjectAndFkauthor(Subject subject, WebsiteUser author);
    Proposition findByName(String name);
    Proposition findById(int id);
    List<Proposition> findAllByFksubject(Subject subject);
    List<Proposition> findAllByFkauthor(WebsiteUser author);
    List<Proposition> findAllBy();
    List<Proposition> getAllBy();
}
