package com.ai16.project.proposition;

import com.ai16.project.subject.Subject;
import com.ai16.project.websiteUser.WebsiteUser;

import javax.persistence.*;

@Entity
@Table (name = "proposition")
public class Proposition {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    @ManyToOne
    @JoinColumn(name = "fk_subject", nullable = false)
    private Subject fksubject;
    @ManyToOne
    @JoinColumn(name = "fk_author", nullable = false)
    private WebsiteUser fkauthor;
    private String name;
    private String state;
    private String comment;
/*
    @OneToMany(mappedBy="proposition")
    private Set<Consultation> consultations;
*/
    public Proposition(WebsiteUser author, Subject subject, String name) {
        this.fksubject = subject;
        this.fkauthor = author;
        this.name = name;
        this.state = "Active";
    }

    public Proposition() {
    }

    public Proposition(Subject subject, WebsiteUser author, String state, String comment) {
        this.fksubject = subject;
        this.fkauthor = author;
        if (state == "Active" || state =="Inactive") this.state = state;
        else this.state = "Active";
        this.comment = comment;
    }

    public void setState(String state) { this.state = state; }
    public void setComment(String comment) { this.comment = comment; }

    public int getId() { return id; }
    public Subject getFksubject() { return fksubject; }
    public WebsiteUser getFkauthor() {  return fkauthor; }
    public String getName() { return name; }
    public String getState() { return state; }
    public String getComment() { return comment; }
}
