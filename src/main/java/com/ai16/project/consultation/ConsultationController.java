package com.ai16.project.consultation;

import com.ai16.project.proposition.Proposition;
import com.ai16.project.proposition.PropositionRepository;
import com.ai16.project.subject.SubjectRepository;
import com.ai16.project.websiteUser.WebsiteUser;
import com.ai16.project.websiteUser.WebsiteUserRepository;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/yfc_utc/consultations")
public class ConsultationController {
    private ConsultationRepository consultationRepository;
    private PropositionRepository propositionRepository;
    private WebsiteUserRepository userRepository;

    public ConsultationController (ConsultationRepository consultationRepository, WebsiteUserRepository userRepository, PropositionRepository propositionRepository, SubjectRepository subjectRepository) {
        this.consultationRepository = consultationRepository;
        this.userRepository = userRepository;
        this.propositionRepository = propositionRepository;
    }


    /*
        get all consultations
     */
    @GetMapping(value = "")
    public List<Consultation> get() {
        List<Consultation> consultations = consultationRepository.getAllBy();

        return consultations;
    }

    /*
        Post many consultations
     */
    @PostMapping(value = "")
    public List<Consultation> postSome(@RequestBody String toParse) {
        JSONArray jsonArray = new JSONArray(toParse);
        List<Consultation> consultations = new ArrayList<>();


        for (int i = 0; i < jsonArray.length(); i++) {
            JSONObject jsonObject = jsonArray.getJSONObject(i);

            Proposition proposition = propositionRepository.findByName(jsonObject.getString("proposition"));
            WebsiteUser user = userRepository.findByUsername(jsonObject.getString("user"));
            if (consultationRepository.findByFkuserAndFkproposition(user, proposition) == null) {
                Consultation consultation = new Consultation(user, proposition);

                consultationRepository.save(consultation);
                consultations.add(consultation);
            }
        }

        return consultations;
    }

    /*
        Post a consultation by its id
     */
    @PostMapping(value = "/consultation")
    public Consultation post(@RequestBody String toParse) {
        JSONObject jsonObject = new JSONObject(toParse);

        Proposition proposition = propositionRepository.findByName(jsonObject.getString("proposition"));
        WebsiteUser user = userRepository.findByUsername(jsonObject.getString("user"));
        if (consultationRepository.findByFkuserAndFkproposition(user, proposition) == null) {
            Consultation consultation = new Consultation(user, proposition);

            consultationRepository.save(consultation);

            return consultation;
        } else { return null; }
    }
}
