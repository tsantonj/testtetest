package com.ai16.project.consultation;

import com.ai16.project.proposition.Proposition;
import com.ai16.project.websiteUser.WebsiteUser;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ConsultationRepository extends JpaRepository<Consultation, Long> {
    Consultation findByFkuserAndFkproposition(WebsiteUser fkuser, Proposition fkproposition);
    List<Consultation> findAllByFkproposition(Proposition fkproposition);
    List<Consultation> findAllByFkuser(WebsiteUser fkuser);
    List<Consultation> getAllBy();

}


