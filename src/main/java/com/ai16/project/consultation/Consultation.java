package com.ai16.project.consultation;

import com.ai16.project.proposition.Proposition;
import com.ai16.project.websiteUser.WebsiteUser;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "consultation")
public class Consultation {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    @ManyToOne
    @NotNull
    @JoinColumn(name = "fkuser")
    private WebsiteUser fkuser;
    @ManyToOne
    @NotNull
    @JoinColumn(name = "fkproposition")
    private Proposition fkproposition;
    /*
    @OneToMany(mappedBy="consultation")
    private Set<Response> responses;
*/
    public Consultation(WebsiteUser user, Proposition proposition) {
        this.fkuser = user;
        this.fkproposition = proposition;
    }

    public Consultation() {
    }

    public int getId() {
        return id;
    }

    public WebsiteUser getFkuser() {
        return fkuser;
    }

    public Proposition getFkproposition() {
        return fkproposition;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setFkuser(WebsiteUser fkuser) {
        this.fkuser = fkuser;
    }

    public void setFkproposition(Proposition fkproposition) {
        this.fkproposition = fkproposition;
    }
}
