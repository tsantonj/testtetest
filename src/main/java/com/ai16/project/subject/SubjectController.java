package com.ai16.project.subject;
import com.ai16.project.proposition.Proposition;
import com.ai16.project.proposition.PropositionRepository;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;


@RestController
@RequestMapping("/yfc_utc/subjects")
public class SubjectController /*implements Consumer<SearchCriteria>*/ {
    private SubjectRepository subjectRepository;
    private PropositionRepository propositionRepository;

    public SubjectController(SubjectRepository subjectRepository, PropositionRepository propositionRepository) {
        this.subjectRepository = subjectRepository;
        this.propositionRepository = propositionRepository;
    }

    /*
        get all subjects
     */
    @GetMapping(value = "")
    public List<Subject> get() {
        List<Subject> subjects = subjectRepository.getAllBy();

        return subjects;
    }

    /*
        get a subject by its id
     */
    @GetMapping(value = "/{id}")
    public Subject get(@PathVariable("id") int id) {
        return subjectRepository.findById(id);
    }

    /*
        get all propositions linked to that subject
     */
    @GetMapping(value = "/{id}/propositions")
    public List<Proposition> getPropositions(@PathVariable("id") int id) {
        Subject subject = subjectRepository.findById(id);
        List<Proposition> propositions = propositionRepository.findAllByFksubject(subject);
        return propositions;
    }

    /*
        request
     *//*
    @RequestMapping(method = RequestMethod.GET, value = "")
    @ResponseBody
    public List<Subject> findAll(@RequestParam(value = "search", required = false) String search) {
        List<SearchCriteria> params = new ArrayList<SearchCriteria>();
        if (search != null) {
            Pattern pattern = Pattern.compile("(\w+?)(:|<|>)(\w+?),");
            Matcher matcher = pattern.matcher(search + ",");
            while (matcher.find()) {
                params.add(new SearchCriteria(matcher.group(1),
                        matcher.group(2), matcher.group(3)));
            }
        }
        return api.searchSubject(params);
    }*/

    /*
        Post many subjects
     */
    @PostMapping(value = "")
    public List<Subject> postSome(@RequestBody String toParse) {
        JSONArray jsonArray = new JSONArray(toParse);
        List<Subject> subjects = new ArrayList<>();

        for (int i = 0; i < jsonArray.length(); i++) {
            JSONObject jsonObject = jsonArray.getJSONObject(i);
            String name = jsonObject.getString("name");

            if (subjectRepository.findByName(name) == null) {
                String description = jsonObject.getString("description");

                Subject subject = new Subject(name, description);
                subjects.add(subject);
                subjectRepository.save(subject);
            } else {
                System.err.println("This subject already exists : " + name);
            }
        }

        return subjects;
    }

    /*
        Post a subject by its id
     */
    @PostMapping(value = "/subject")
    public Subject post(@RequestBody String toParse) {
        JSONObject jsonObject = new JSONObject(toParse);
        String name = jsonObject.getString("name");

        if (subjectRepository.findByName(name) != null) {
            return null;
        } else {
            Subject new_subject = new Subject(name, jsonObject.getString("description"));
            subjectRepository.save(new_subject);
            return new_subject;
        }
    }

    @PutMapping(value = "")
    public List<Subject> putMany(@RequestBody String toParse) {
        JSONArray jsonArray = new JSONArray(toParse);
        List<Subject> subjects = new ArrayList<>();

        for (int i = 0; i < jsonArray.length(); i++) {
            JSONObject jsonObject = jsonArray.getJSONObject(i);

            String name = jsonObject.getString("name");
            Subject subject = subjectRepository.findByName(name);
            subject.setDescription(jsonObject.getString("address"));

            subjectRepository.save(subject);
            subjects.add(subject);
        }

        return subjects;
    }

    @PutMapping(value = "/{id}")
    public Subject put(@PathVariable("id") int id, @RequestBody String toParse) {
        Subject subject =  subjectRepository.findById(id);
        JSONObject jsonObject = new JSONObject(toParse);

        subject.setDescription(jsonObject.getString("description"));

        subjectRepository.save(subject);

        return subject;
    }
}
