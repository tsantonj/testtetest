package com.ai16.project.subject;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface SubjectRepository extends JpaRepository<Subject, Long> {
    Subject findByName(String name);
    Subject findById(int id);
    List<Subject> getAllBy();
}
